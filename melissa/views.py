from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from melissa.common import Common, Utility
from melissa.serializers import NewOwnerSerializer, CheapestZipCodeSerializer
from melissa.filters import NewOwnerFilterBackend, CheapestFilterBackend


class NewHomeOwner(APIView):
    utility = Utility()
    common = Common()
    filter_backends = (NewOwnerFilterBackend,)

    def get(self, request):
        params = request.query_params
        zip_code = params.get('zipcode')
        pub_year_month = params.get('pubyearmonth')
        serializer = NewOwnerSerializer(data=dict(zip_code=zip_code, pub_year_month=pub_year_month))
        if serializer.is_valid(raise_exception=True):
            response, api_status = self.common.get_request(zip_code=zip_code, pub_year_month=pub_year_month)
            if api_status == status.HTTP_200_OK:
                if pub_year_month:
                    response = self.utility.filter_response_by_month(response, pub_year_month)
        return Response(response, status=api_status)


class CheapestLocation(APIView):
    utility = Utility()
    common = Common()
    filter_backends = (CheapestFilterBackend,)

    def get(self, request):
        params = request.query_params

        zip_code = params.get('zipcodes')
        pub_year_month = params.get('pubyearmonth')
        zipcodes = zip_code.split(',')

        serializer = CheapestZipCodeSerializer(data=dict(zipcodes=zipcodes, pubyearmonth=pub_year_month))

        if serializer.is_valid(raise_exception=True):
            result = []
            for zipcode in set(zipcodes):
                response, api_status = self.common.get_request(zip_code=zipcode, pub_year_month=pub_year_month)
                if api_status == status.HTTP_200_OK:
                    result_filter_by_month = self.utility.filter_response_by_month(response, pub_year_month)
                    if result_filter_by_month:
                        result.extend(result_filter_by_month)

            if len(result) > 0:
                result = self.utility.filter_cheapest_location(result)
            return Response(result, status=status.HTTP_200_OK)
