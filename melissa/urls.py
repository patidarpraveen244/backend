from django.urls import path

from melissa.views import NewHomeOwner, CheapestLocation

urlpatterns = [
    path('new-home-owner', NewHomeOwner.as_view(), name='new-home-owner'),
    path('get-cheapest-location', CheapestLocation.as_view(), name='get-cheapest-location'),
]
