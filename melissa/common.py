import os
import json
import requests
from operator import itemgetter


class Common:
    MELISSA_URL = os.getenv('MELISSA_URL')
    MELISSA_ID = os.getenv('MELISSA_ID')

    def set_api_params(self, zip_code, pub_year_month=''):
        params = dict()
        params["id"] = self.MELISSA_ID
        params["zipcode"] = zip_code
        params["pubyearmonth"] = pub_year_month
        return params

    def get_request(self, zip_code, pub_year_month=''):
        try:
            params = self.set_api_params(zip_code=zip_code, pub_year_month=pub_year_month)
            response = requests.get(self.MELISSA_URL, params)
            return response.json(), 200
        except json.decoder.JSONDecodeError:
            return dict(
                message="Something went wrong",
                description="*** json.decoder.JSONDecodeError: Expecting value: line 2 column 1 (char 2)"), 400


class Utility:

    def filter_response_by_month(self, data, pub_year_month):
        month = int(pub_year_month.split('/')[0])
        year = int(pub_year_month.split('/')[1])
        return [month_data for month_data in data if month_data
                if month_data.get('PubMonth') == month and month_data.get('PubYear') == year]

    def filter_cheapest_location(self, data):
        sorted_zipcodes = sorted(data, key=itemgetter('AvgPrice'))
        return sorted_zipcodes[0]

    def is_invalid_pub_year_month(self, pub_year_month):
        if (('/' not in pub_year_month) or
                not ('/' in pub_year_month and 1 <= len(pub_year_month.split('/')[0]) <= 2 and
                     len(pub_year_month.split('/')[1]) == 4)):
            return True
        return False
