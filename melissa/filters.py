import coreapi
from rest_framework.filters import BaseFilterBackend


class NewOwnerFilterBackend(BaseFilterBackend):
    def get_schema_fields(self, view):
        return [coreapi.Field(
            name='zipcode',
            location='query',
            required=True,
            type='string',
            description='Zipcode e.g.zipcode=90201'
        ),
            coreapi.Field(
                name='pubyearmonth',
                location='query',
                required=False,
                type='string',
                description='e.g.:- pubyearmonth=11/2019'
            )
        ]


class CheapestFilterBackend(BaseFilterBackend):
    def get_schema_fields(self, view):
        return [coreapi.Field(
            name='zipcodes',
            location='query',
            required=True,
            type='string',
            description='Zipcodes e.g.zipcodes=90201, 73099, 34668,37042'
        ),
            coreapi.Field(
                name='pubyearmonth',
                location='query',
                required=True,
                type='string',
                description='e.g.:- pubyearmonth=11/2019'
            )
        ]
