from rest_framework import serializers

from melissa.common import Utility


class NewOwnerSerializer(serializers.Serializer):
    zip_code = serializers.CharField(max_length=10)
    pub_year_month = serializers.CharField(max_length=10, allow_null=True, required=False)

    def validate(self, attrs):
        zip_code = attrs.get('zip_code')
        pub_year_month = attrs.get('pub_year_month')
        if len(zip_code) != 5:
            '''
            Enter a 5-Digit ZIP Code: mentioned in webpage 
            https://www.melissa.com/v2/lookups/newhomeowner/?zipcode=90201&pubyearmonth= 
            '''
            raise serializers.ValidationError("Zip Code is wrong, please enter correct zipcode.")
        if pub_year_month and Utility().is_invalid_pub_year_month(pub_year_month=pub_year_month):
            raise serializers.ValidationError("year_month format is wrong it should be like. 11/2019")
        return attrs


class StringListField(serializers.ListField):
    child = serializers.CharField(min_length=5, max_length=5)


class CheapestZipCodeSerializer(serializers.Serializer):
    zipcodes = StringListField()
    pubyearmonth = serializers.CharField(max_length=10)

    def validate(self, attrs):
        pub_year_month = attrs.get('pubyearmonth')
        if pub_year_month and Utility().is_invalid_pub_year_month(pub_year_month=pub_year_month):
            raise serializers.ValidationError("year_month format is wrong it should be like. 11/2019")
        return attrs
