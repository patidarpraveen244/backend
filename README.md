## Melissa API 

### Prerequisites


```
python3 (3.7.5)
```
Create virtual environment using python 1

**Linux**

```
python3 -m venv env
source env/bin/activate
pip3 install -r requirements.txt
```

Add **.env** file in base directory of the project

```
export MELISSA_URL=https://www.melissa.com/v2/lookups/newhomeowner/?fmt=json&
export MELISSA_ID=<<Pass License Key Using Credits of your Melissa account>>
```
and run following command to export the environment variables.
```
source .env
```

To start the Django server use:
```
python manage.py runserver
```

The API's created are:
1. To get the average home value of a given month and zip code (eg: sep2020)
> URL : http://127.0.0.1:8000/api/new-home-owner?zipcode=90201&pubyearmonth=11/2019
METHOD : GET

This will return the value of averge price according to the given zipcode and month/year.

2. To find the cheapest location(zip code) for a given month, given a list of zipcodes from

> URL : http://127.0.0.1:8000/api/get-cheapest-location?zipcode=90201&pubyearmonth=11/2019
METHOD : GET

This will return the cheapest location from the provided zipcode and month.

3. Time Profiler: For this I have used [django-silk](https://pypi.org/project/django-silk/0.5.2/) package, you can use the following URL to get the information
 > http://127.0.0.1:8000/silk/

